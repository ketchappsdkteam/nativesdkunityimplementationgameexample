//
//  Interstitiel.h
//
//  Created by Michel Morcos on 13/07/13.
//  Modified by Pierre Julien Baron (Fluffy_Kaeloky) on 15/10/2018
//
////

#import <UIKit/UIKit.h>
#import <StoreKit/SKStoreProductViewController.h>
#import <Mediaplayer/Mediaplayer.h>
//@import MediaPlayer;
#import <AVFoundation/AVFoundation.h>
#import <SystemConfiguration/SystemConfiguration.h>
//#import <AVKit/AVKit.h>
#import <AdSupport/AdSupport.h>
#import "Square.h"

typedef enum
{
    GDPR_ACCEPTED = 0,
    GDPR_REFUSED = 1,
    GDPR_FIRST_LAUNCH = 2
} GDPRStatus;

@class Interstitiel;             //define class, so protocol can see MyClass
@protocol InterstitielDelegate   //define delegate protocol
- (void) interstitielDidFailToLoad;  //define delegate method to be
- (void) interstitielDidLoad: (Interstitiel *) sender withvideo:(BOOL) hasvideo;  //define delegate method to be
- (void) interstitielDidClose: (Interstitiel *) sender withvideo:(BOOL) hasvideo;  //define delegate method to be implemented within another class
- (void) squareIsReady;
- (void) isGdpr:(BOOL) optin;


@end //end protocol

@protocol UICheckboxDelegate;

@interface Interstitiel : UIView <SquareDelegate, SKStoreProductViewControllerDelegate, UICheckboxDelegate/*, NSURLConnectionDelegate, NSURLConnectionDownloadDelegate, NSURLConnectionDataDelegate*/>
{
    
    Square *theSquare;
    
	NSMutableDictionary *adDico;
    NSMutableDictionary *langDico;
    
    NSUserDefaults *standardUserDefaults;
    
    NSString *theAdId;
    
    bool openAd;
    bool playvideo;
    bool isSurvey;
    
    UIActivityIndicatorView *loadingAlert;
    
    UIView *generalView;
    UIView *bgdview;
    //UIButton *adButton;
    //UIImageView *imgValidate;
    //UIButton *closeButton;
    
    NSString *appName;
    NSString *orientation;
    
    NSTimeInterval start;
    
    //double time_speed;
    bool isClosing;
    bool storeLoad;
    bool openad;
    NSTimer *_timer;
    NSURLConnection *conn;
    
    CGRect videoFrame;
    CGRect windowFrame;
    
    UIScrollView *gdprscrollView;
    
    //double time_speed;
    
    NSMutableArray *paramArraySave;
    
    GDPRStatus isGdpr;
    
    SKStoreProductViewController *storeControllersaved;
}

//@property (nonatomic, assign) UIViewController *rootViewController;

@property (nonatomic, assign) id <InterstitielDelegate> delegate; //define MyClassDelegate as delegate
@property(strong,nonatomic)MPMoviePlayerController *moviePlayer;
//@property(strong,nonatomic) AVPlayerViewController *moviePlayer;


//@property(nonatomic,retain) NSDictionary *adDico;

- (void) displaySite;
- (void) displaySiteVideo;
- (bool) hasAd;
- (void) createAd;
- (void) show:(UIView*) topview;
- (void) showSquare:(UIView*) topview atPoint:(CGPoint) thePoint;
- (id)initWithFrame:(CGRect)frame appName:(NSString*)theappName orientation:(NSString*)theorientation;
-(void) fetchSquare;
-(void) removeSquare;
-(void) openGdprWindow;
-(BOOL) isCountryGdpr;
-(GDPRStatus) isGdprOptin;
-(NSUInteger) supportedInterfaceOrientations;
-(BOOL) shouldAutorotate;
-(BOOL) prefersStatusBarHidden;
- (void) OnCheckedStateChanged: (bool)value;

@end
