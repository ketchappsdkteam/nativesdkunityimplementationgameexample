//
//  Square.h
//  
//
//  Created by Michel Morcos on 31/10/2016.
//
//

#import "Reachability.h"
#import <UIKit/UIKit.h>
#import <StoreKit/SKStoreProductViewController.h>
#import <Mediaplayer/Mediaplayer.h>
#import <AVFoundation/AVFoundation.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import <AdSupport/AdSupport.h>

#import <Foundation/Foundation.h>

@class Square;             //define class, so protocol can see MyClass
@protocol SquareDelegate   //define delegate protocol
- (void) squareIsReady;


@end //end protocol

@interface Square : UIView <SKStoreProductViewControllerDelegate>
{
    NSMutableDictionary *adDico;

    NSUserDefaults *standardUserDefaults;
    
    NSString *appName;	
    NSString *theAdId;
    
    bool storeLoad;
    bool isClosing;
    bool playvideo;
    bool openad;
    bool isFetching;
    
    CGRect videoFrame;
    CGRect windowFrame;
    
    UIView *generalView;
    UIView *bgdview;
    
    NSMutableArray *paramArraySave;
    
    UIImage *theImageVideo;
    
    bool isGdpr;
    
    bool alreadyload;
    
}

@property (nonatomic, assign) id <SquareDelegate> delegate;
@property (strong,nonatomic) MPMoviePlayerController *moviePlayer;
@property (strong,nonatomic) AVPlayer *avMoviePlayer;

- (id)initWithFrame:(CGRect)frame appName:(NSString*)theappName theID:(NSString*)adI;
- (void) showSquareFinal:(UIView*) topview atPoint:(CGPoint) thePoint;
- (void) fetchSquare;
- (void) removeSquare;

- (void) setGdpr:(bool) result;

- (NSUInteger) supportedInterfaceOrientations;
- (BOOL) shouldAutorotate;

- (void) productViewControllerDidFinish:(SKStoreProductViewController *)viewController;

@end
