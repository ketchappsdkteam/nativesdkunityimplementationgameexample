//
//  Ketchapp.h
//  Ketchapp
//
//  Created by Michel Morcos on 16/08/2015.
//  Copyright (c) 2015 Ketchapp. All rights reserved.
//  Version 1.2.1

#import <UIKit/UIKit.h>


//! Project version number for Ketchapp.
FOUNDATION_EXPORT double KetchappVersionNumber;

//! Project version string for Ketchapp.
FOUNDATION_EXPORT const unsigned char KetchappVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Ketchapp/PublicHeader.h>

#import "Interstitiel.h"
#import "Reachability.h"

