
//
//  KetchappController.mm
//

#import "UnityAppController.h"
#import "UnityAppController+ViewHandling.h"

#import <Ketchapp/Ketchapp.h>

extern void UnitySendMessage(const char *, const char *, const char *);

@interface KetchappController : UnityAppController<InterstitielDelegate>
{
    Interstitiel *theAd;
}
@end

@implementation KetchappController

- (void)initialize
{
	if (theAd == nil)
	{
		theAd = [[Interstitiel alloc] initWithFrame:[[UIScreen mainScreen] bounds] appName:@"com.ketchapp.yourgamename" orientation:@"portrait"];
		theAd.delegate = self;
	}
}

- (void)showKetchappPromo
{
	[theAd createAd];
}

- (void) interstitielDidLoad:(Interstitiel*) theInterstitiel withvideo:(BOOL)hasvideo
{   
    if(theInterstitiel != nil)
    {
        //If hasvideo is Yes then pause the sound
        
        [theInterstitiel show:self.rootView];
        
        if(hasvideo)
            UnitySendMessage("__KetchappIOSCallbackReceiver", "InterstitielDidLoad", "true");
        else
            UnitySendMessage("__KetchappIOSCallbackReceiver", "InterstitielDidLoad", "false");
    }
}

-(void) interstitielDidFailToLoad
{
    //If there is no cross promo (don't use that to display the banner)
    
    UnitySendMessage("__KetchappIOSCallbackReceiver", "InterstitielDidFailToLoad", "");
    
}

- (void) interstitielDidClose:(Interstitiel*) theInterstitiel withvideo:(BOOL)hasvideo
{
    //the user has close the cross promo
    //If hasvideo is Yes then play the sound back
    
    if(hasvideo)
        UnitySendMessage("__KetchappIOSCallbackReceiver", "InterstitielDidClose", "true");
    else
        UnitySendMessage("__KetchappIOSCallbackReceiver", "InterstitielDidClose", "false");
    
}

- (void) isGdpr:(BOOL)optin
{
    //the user has close the cross promo
    //If hasvideo is Yes then play the sound back
    
    if(optin)
        UnitySendMessage("__KetchappIOSCallbackReceiver", "IsGdpr", "true");
    else
        UnitySendMessage("__KetchappIOSCallbackReceiver", "IsGdpr", "false");
    
}


-(void) fetchKetchappSquare
{
    [theAd fetchSquare];
}

-(void) showKetchappSquare:(float) normalizedPositionX andPosition:(float) normalizedPositionY
{
    CGRect screenBounds = [[UIScreen mainScreen]bounds];
    CGFloat pointX = screenBounds.size.width * normalizedPositionX;
    CGFloat pointY = screenBounds.size.height * normalizedPositionY;

    [theAd showSquare:self.rootView atPoint:CGPointMake(pointX, pointY)];
}

- (void) removeKetchappSquare
{
    [theAd removeSquare];
}

- (void) openKetchappGdprWindow
{
    [theAd openGdprWindow];
}

- (BOOL) isKetchappCountryGdpr
{
    return [theAd isCountryGdpr];
}

-(void) squareIsReady
{
    //After fetching this methode is called to let you know there is a square cross promo for game over screen.
    //Use this on the game over screen : [theAd showSquare:theView atPoint:CGPointMake(x,y)];
    
    UnitySendMessage("__KetchappIOSCallbackReceiver", "SquareIsReady", "");
}

-(int) isGdprOptin
{
	return [theAd isGdprOptin];
}

@end

extern "C" void _Initialize()
{
	[(KetchappController*)GetAppController () initialize];
}

extern "C" void _ShowKetchappPromo()
{
    [(KetchappController*)GetAppController () showKetchappPromo];
}

extern "C" void _FetchKetchappSquare()
{
    [(KetchappController*)GetAppController () fetchKetchappSquare];
}

extern "C" void _ShowKetchappSquare(float normalizedPositionX, float normalizedPositionY)
{
	[(KetchappController*)GetAppController () showKetchappSquare:normalizedPositionX andPosition:normalizedPositionY];
}

extern "C" void _RemoveKetchappSquare()
{
    [(KetchappController*)GetAppController () removeKetchappSquare];
}

extern "C" void _OpenKetchappGdprWindow()
{
    [(KetchappController*)GetAppController () openKetchappGdprWindow];
}

extern "C" bool _IsKetchappCountryGdpr()
{
    return [(KetchappController*)GetAppController () isKetchappCountryGdpr];
}

extern "C" int _IsGDPROptIn()
{
    return [(KetchappController*)GetAppController() isGdprOptin];
}



IMPL_APP_CONTROLLER_SUBCLASS(KetchappController)
