﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class ParallaxBackground : MonoBehaviour
{
    public Transform movementTarget = null;
    public float moveFactor = 1.0f;

    private Image image = null;

    private Vector3 oldTargetPosition = Vector3.zero;

    private void Awake()
    {
        image = GetComponent<Image>();
    }

    private void Start()
    {
        oldTargetPosition = movementTarget.position;
    }

    private void Update()
    {
        Vector3 currentTargetPosition = movementTarget.position;

        Vector3 delta = currentTargetPosition - oldTargetPosition;

        Vector2 materialTextureOffset = image.material.mainTextureOffset;

        image.material.mainTextureOffset = new Vector2(Mathf.Repeat(materialTextureOffset.x + delta.x * moveFactor * Time.deltaTime, 1.0f), materialTextureOffset.y);

        oldTargetPosition = currentTargetPosition;
    }
}
