﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController2D))]
public class PlatformDisabler : MonoBehaviour
{
    public Joystick joystick = null;

    public float disableDelay = 0.2f;

    private bool downPressed = false;

    private CharacterController2D characterController = null;

    private void Update()
    {
#if UNITY_EDITOR
        downPressed = Input.GetKey(KeyCode.DownArrow);
#else
        downPressed = joystick.Vertical < -0.5f;
#endif
        characterController = GetComponent<CharacterController2D>();
    }

    private void FixedUpdate()
    {
        if (downPressed)
        {
            Collider2D[] colliders = Physics2D.OverlapCircleAll(characterController.GroundCheckTransform.position, 0.3f, characterController.GroundLayerMask);
            foreach (Collider2D collider in colliders)
            {
                PlatformEffector2D effector = collider.gameObject.GetComponentInParent<PlatformEffector2D>();
                if (effector != null)
                {
                    collider.enabled = false;
                    StartCoroutine(ReactivatePlatformDelayed(collider, disableDelay));
                }
            }
        }
    }

    private IEnumerator ReactivatePlatformDelayed(Collider2D collider, float delay)
    {
        yield return new WaitForSeconds(delay);

        collider.enabled = true;
        CompositeCollider2D compositeCollider2D = collider.gameObject.GetComponent<CompositeCollider2D>();
        if (compositeCollider2D != null)
            compositeCollider2D.GenerateGeometry();
    }
}
