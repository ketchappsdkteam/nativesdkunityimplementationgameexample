﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Light))]
public class LightFlicker : MonoBehaviour
{
    public float flickerDelay = 0.1f;

    public float minIntensity = 8.0f;
    public float maxIntensity = 13.0f;

    private new Light light = null;

    private void Awake()
    {
        light = GetComponent<Light>();
    }

    private void Start()
    {
        StartCoroutine(UpdateLoop());
    }

    private IEnumerator UpdateLoop()
    {
        while (true)
        {
            yield return new WaitForSeconds(flickerDelay);

            light.intensity = Random.Range(minIntensity, maxIntensity);
        }
    }
}
