﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(CharacterController2D))]
public class PlayerController : GameStateChangedListener
{
    public Joystick joystick = null;
    public UIButtonState button = null;

    private CharacterController2D playerMovement = null;

    protected override void Awake()
    {
        base.Awake();

        playerMovement = GetComponent<CharacterController2D>();
    }

    private void FixedUpdate()
    {
        float horizontal = 0.0f;
        bool jump = false;

#if !UNITY_EDITOR
        horizontal = Mathf.Abs(joystick.Horizontal) > 0.2f ? Mathf.Sign(joystick.Horizontal) : 0.0f;
        jump = button.IsPressed;
#else
        horizontal = Input.GetAxis("Horizontal");
        jump = Input.GetButtonDown("Jump") || Input.GetKeyDown(KeyCode.UpArrow);
#endif

        playerMovement.Move(horizontal, false, jump);
    }

    protected override void OnGameStateChanged(GameManager.OnGameStateChangedArgs args)
    {
        if (args.NewState == GameManager.GameStateEnum.GameOver)
            enabled = false;
    }
}
