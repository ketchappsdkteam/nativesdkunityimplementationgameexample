﻿using System.Collections;
using System.Collections.Generic;
using Ketchapp;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class UISettingsScreen : KetchappCallbackBehaviour
{
    public string showAnimatorParameterName = "IsShown";

    private Animator animator = null;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    public void Show()
    {
        animator.SetBool(showAnimatorParameterName, true);
    }

    public void Close()
    {
        animator.SetBool(showAnimatorParameterName, false);
    }

    public void OnOpenGDPRButtonPressed()
    {
        //Better for reading GDPR
        Screen.orientation = ScreenOrientation.Portrait;

        //Cut the sound
        SoundController.SetAudioState(false);

        //Show GDPR
        KetchappPromo.ShowGDPR();
    }

    public void OnBackButtonPressed()
    {
        Close();
    }

    //This is called when the promotion is shown, or the GDPR accepted/refused
    public override void OnGDPRResult(KetchappPromo.OnGDPRResultArgs args)
    {
        //Reset game orientation
        Screen.orientation = ScreenOrientation.LandscapeLeft;

        //Re-enable the sound
        SoundController.SetAudioState(true);
    }
}
