﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class UIRainbowText : MonoBehaviour
{
    public float rainbowSpeed = 1.0f;

    private Text text = null;

    private void Awake()
    {
        text = GetComponent<Text>();
    }

    private void Update()
    {
        text.color = Color.HSVToRGB(Mathf.Repeat(Time.time * rainbowSpeed, 1.0f), 1.0f, 1.0f);
    }
}
