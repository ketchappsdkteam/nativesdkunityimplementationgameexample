﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class UISinusoidalText : MonoBehaviour, IMeshModifier
{
    public float amplitude = 1.0f;
    public float waveSpeed = 1.0f;
    public float waveLength = 1.0f;

    public void ModifyMesh(Mesh mesh)
    {
    }

    public void ModifyMesh(VertexHelper verts)
    {
        Mesh mesh = new Mesh();
        verts.FillMesh(mesh);

        verts.Clear();

        Vector3[] vertices = mesh.vertices;

        for (int i = 0; i < mesh.vertexCount; i++)
            vertices[i].y = vertices[i].y + Mathf.Sin((Time.time * waveSpeed) + (vertices[i].x * waveLength)) * amplitude;

        for (int i = 0; i < mesh.vertexCount; i++)
            verts.AddVert(vertices[i], mesh.colors[i], mesh.uv[i]);

        for (int i = 0; i < mesh.triangles.Count(); i += 3)
            verts.AddTriangle(mesh.triangles[i], mesh.triangles[i + 1], mesh.triangles[i + 2]);
    }
}