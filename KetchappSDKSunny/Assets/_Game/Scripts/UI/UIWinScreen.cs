﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[RequireComponent(typeof(Animator))]
public class UIWinScreen : GameStateChangedListener
{
    public string showScreenAnimatorParameterName = "Show";

    public List<Image> stars = new List<Image>();

    public Sprite starFull = null;
    public Sprite starEmpty = null;

    public Text timeText = null;

    public UISettingsScreen settingsScreen = null;

    public UnityEvent onPanelShown = new UnityEvent();

    private Animator animator = null;

    private string timeBaseText = "";

    protected override void Awake()
    {
        base.Awake();

        animator = GetComponent<Animator>();

        timeBaseText = timeText.text;
    }

    protected override void OnGameStateChanged(GameManager.OnGameStateChangedArgs args)
    {
        if (args.NewState == GameManager.GameStateEnum.GameOver)
            animator.SetTrigger(showScreenAnimatorParameterName);

        float finalTime = LevelScoreManager.Instance.CurrentTime;

        //Time text
        int minutes = (int)finalTime / 60;
        int seconds = (int)finalTime % 60;

        timeText.text = string.Format(timeBaseText, minutes, seconds);

        //Stars
        stars[0].sprite = finalTime <= LevelScoreManager.Instance.oneStarTime ? starFull : starEmpty;
        stars[1].sprite = finalTime <= LevelScoreManager.Instance.twoStarsTime ? starFull : starEmpty;
        stars[2].sprite = finalTime <= LevelScoreManager.Instance.threeStarsTime ? starFull : starEmpty;

        if (onPanelShown != null)
            onPanelShown.Invoke();
    }

    public void OnReplayButtonPressed()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void OnSettingsButtonPressed()
    {
        settingsScreen.Show();
    }
}
