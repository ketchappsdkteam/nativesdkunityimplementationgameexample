﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ketchapp;

public class UIKetchappSquareManager : KetchappCallbackBehaviour
{
    //This is our newly created RectTransform.
    public Transform squareTransform = null;

    public int gameOverCooldown = 1;

    private bool squareFetched = false;

    private static int cooldown = 0;

    private void Awake()
    {
        //Remove the square on scene loading.
        ResetSquare();

        if (cooldown <= 0)
        {
            Debug.Log("UIKetchappSqaureManager > Square Fetched.");
            KetchappPromo.FetchSquare();

            cooldown = gameOverCooldown;
        }
        else
            --cooldown;
    }

    //This is fired when the menu stops animating, and the square transform should be at it's final position.
    public void NotifyEndAnimation()
    {
        //If we are indeed in game-over state, then show that square.
        if (GameManager.Instance.GameState == GameManager.GameStateEnum.GameOver && squareFetched)
            StartCoroutine(ShowSquare());
    }

    public void ResetSquare()
    {
        Debug.Log("UIKetchappSquareManager > Removed square.");
        //Remove the square.
        KetchappPromo.RemoveSquare();

        squareFetched = false;
    }

    public IEnumerator ShowSquare()
    {
        //Wait until the square was actually fetched.
        yield return new WaitUntil(() => { return squareFetched; });

        Debug.Log("UIKetchappSquareManager > Square Shown.");
        KetchappPromo.ShowSquare(squareTransform.position);
    }

    //This is automatically called once the square has been fetched and is ready to be shown.
    public override void OnSquareReady()
    {
        Debug.Log("UIKetchappSquareManager > OnSquareReady Invoked.");
        //Just rememeber that it was fetched.
        squareFetched = true;
    }
}
