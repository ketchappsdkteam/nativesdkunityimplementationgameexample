﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Animator))]
public class UIGemCounter : MonoBehaviour
{
    public string feedbackAnimatorParameter = "Feedback";

    public Text uiText = null;

    private Animator animator = null;

    private string baseUIText = "";

    private int totalGemCount = 0;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    private void Start()
    {
        GameManager.Instance.onGemTaken.AddListener(OnGemTaken);
        baseUIText = uiText.text;
        totalGemCount = GameManager.Instance.Gems.Count;

        uiText.text = string.Format(baseUIText, totalGemCount - GameManager.Instance.Gems.Count, totalGemCount);
    }

    private void OnGemTaken(GemController.OnGemTakenArgs args)
    {
        uiText.text = string.Format(baseUIText, totalGemCount - GameManager.Instance.Gems.Count, totalGemCount);
        animator.SetTrigger(feedbackAnimatorParameter);
    }
}
