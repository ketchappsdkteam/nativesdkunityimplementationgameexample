﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(CharacterController2D))]
public class CharacterAnimatorController : MonoBehaviour
{
    public string horizontalSpeedParameterName = "HorizontalSpeed";
    public string verticalSpeedParameterName = "VerticalSpeed";
    public string groundedParameterName = "IsGrounded";

    private new Rigidbody2D rigidbody = null;
    private Animator animator = null;
    private CharacterController2D characterController = null;

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        characterController = GetComponent<CharacterController2D>();
    }

    private void Update()
    {
        animator.SetFloat(horizontalSpeedParameterName, Mathf.Abs(rigidbody.velocity.x));
        animator.SetFloat(verticalSpeedParameterName, rigidbody.velocity.y);

        animator.SetBool(groundedParameterName, characterController.IsGrounded);
    }
}
