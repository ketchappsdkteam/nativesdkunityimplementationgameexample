﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class LevelScoreManager : GameStateChangedListener
{
    public static LevelScoreManager Instance { get; private set; } = null;

    public float threeStarsTime = 30.0f;
    public float twoStarsTime = 40.0f;
    public float oneStarTime = 50.0f;

    public float timeMultiplier = 1.0f;

    public float CurrentTime { get; private set; } = 0.0f;

    protected override void Awake()
    {
        base.Awake();

        Instance = this;
    }

    private void Update()
    {
        CurrentTime += Time.deltaTime * timeMultiplier;
    }

    protected override void OnGameStateChanged(GameManager.OnGameStateChangedArgs args)
    {
        if (args.NewState == GameManager.GameStateEnum.GameOver)
            timeMultiplier = 0.0f;
    }
}
