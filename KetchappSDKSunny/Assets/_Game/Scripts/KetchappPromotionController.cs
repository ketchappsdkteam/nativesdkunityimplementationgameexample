﻿/*
 __  ___   _____   _______    ____    _    _       ____      _____   _____
|  |/  /  |  ___| |__   __|  /  __|  | |__| |     / /\ \    |  _  \ |  _  \
|     /   |   |      | |    /  /     |  __  |    / ____ \   |  ___/ |  ___/ 
|     \   |  _|      | |    \  \__   | |  | |   / /    \ \  |  |    |  |    
|__|\__\  |_____|    |_|     \____|  |_|  |_|  |_|      |_| |__|    |__|    

   /\   /\   
  //\\_//\\     ____
  \_     _/    /   /
   / * * \    /^^^]
   \_\O/_/    [   ]
    /   \_    [   /
    \     \_  /  /
     [ [ /  \/ _/
    _[ [ \  /_/

*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ketchapp;
using UnityEngine.SceneManagement;

//Inherit from KetchappCallbackBehaviour to override automatic callbacks.
public class KetchappPromotionController : KetchappCallbackBehaviour
{
    public string gameSceneName = "GameScene";

    public bool noAds = false;

    private void Start()
    {
        Debug.Log("KetchappPromotionController > START");

        //Our game is in landscape, but we want this preloader scene and the ad to be in portrait mode.
        Screen.orientation = ScreenOrientation.Portrait;

        //This is where we request the ad. This is a non-synchronous call. It won't block your thread.
        KetchappPromo.RequestKetchappPromo(noAds);
        //If we have noAds enabled, we directly load the game scene. The PromotionLoadResult won't be called because no ads were loaded.
        if (noAds)
            LoadGameScene();
    }

    //This is called when an ad is shown, whether or not the GPDR shows. It allows you to get the GPDR status.
    public override void OnGDPRResult(KetchappPromo.OnGDPRResultArgs args)
    {
        //Just log the result, we don't have much use for it right now.
        Debug.Log("KetchappPromotionController > OnGDPRResult : Invoked. Accepted : " + args.HasBeenAccepted);
    }

    //This is called when the Promotion was loaded and about to be shown.
    public override void OnPromotionLoadResult(KetchappPromo.OnPromotionResultArgs args)
    {
        Debug.Log("KetchappPromotionController > OnPromotionResult : Invoked. Result : " + args.PromotionResult.ToString());

        //If there was an error, we just skip the ad and go straight to the game.
        if (args.PromotionResult == KetchappPromo.OnPromotionResultArgs.PromotionResultEnum.PROMOTION_FAILED_UNDEFINED_ERROR)
        {
            LoadGameScene();
            return;
        }

        //If the ad is a video, we want to cut the game's sound, as the video probably already contains sound.
        if (args.PromotionResult == KetchappPromo.OnPromotionResultArgs.PromotionResultEnum.PROMOTION_LOADED_WITH_VIDEO)
            SoundController.SetAudioState(false);
    }

    //This is called when the user closes the ad when pressing the exit cross.
    public override void OnPromotionClosed()
    {
        Debug.Log("KetchappPromotionController > OnPromotionClosed : Invoked.");

        //Resume the sound.
        SoundController.SetAudioState(true);
        //Load the game scene.
        LoadGameScene();
    }

    private void LoadGameScene()
    {
        //Go back to our landscape mode.
        Screen.orientation = ScreenOrientation.LandscapeLeft;

        //Leave a few seconds for Unity to rotate the screen, otherwise, the canvas scale on next frame will be corrupted. (Maybe file a bug to Unity for that ?)
        StartCoroutine(LoadSceneDelayed());
    }

    private IEnumerator LoadSceneDelayed()
    {
        //Wait three seconds.
        yield return new WaitForSeconds(3.0f);

        //Load scene.
        SceneManager.LoadScene(gameSceneName);
    }
}
