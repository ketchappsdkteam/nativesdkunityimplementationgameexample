﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GemController : MonoBehaviour
{
    [Serializable]
    public class OnGemTaken : UnityEvent<OnGemTakenArgs> { }

    [Serializable]
    public class OnGemTakenArgs
    {
        public GemController Gem { get; private set; } = null;

        public OnGemTakenArgs(GemController gem)
        {
            Gem = gem;
        }
    }

    public GameObject itemFeedbackPrefab = null;

    public OnGemTaken onGemTaken = new OnGemTaken();

    private void Awake()
    {
        GameManager.Instance.RegisterGem(this);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (itemFeedbackPrefab != null)
            Destroy(Instantiate(itemFeedbackPrefab, transform.position, Quaternion.identity), 0.7f);

        if (onGemTaken != null)
            onGemTaken.Invoke(new OnGemTakenArgs(this));

        Destroy(gameObject);
    }
}
