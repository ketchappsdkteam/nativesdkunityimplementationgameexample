﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class GameStateChangedListener : MonoBehaviour
{
    protected virtual void Awake()
    {
        GameManager.Instance.onGameStateChanged.AddListener(OnGameStateChanged);
    }

    protected virtual void OnGameStateChanged(GameManager.OnGameStateChangedArgs args) { }
}
