﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

public class AnimationTrigger : MonoBehaviour
{
    public UnityEvent onTrigger = new UnityEvent();

    public void TriggerEvent()
    {
        if (onTrigger != null)
            onTrigger.Invoke();
    }
}
