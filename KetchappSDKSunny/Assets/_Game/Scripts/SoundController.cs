﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SoundController
{
    public static void SetAudioState(bool state)
    {
        Debug.Log("Set sound state : " + state);

        AudioListener.volume = state ? 1.0f : 0.0f;
    }
}
