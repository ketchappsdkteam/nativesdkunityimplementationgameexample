﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXManager : MonoBehaviour
{
    public static readonly float DEFAULT_SFX_VOLUME = 0.65f;

    public static SFXManager Instance { get; private set; } = null;

    public AudioSource audioSource = null;

    private void Awake()
    {
        Instance = this;
    }

    public void PlayOneShot(AudioClip clip, float volume)
    {
        audioSource.PlayOneShot(clip, volume);
    }
}
