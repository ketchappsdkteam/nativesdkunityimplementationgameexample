﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class SFXPlayer : MonoBehaviour
{
    public AudioClip clip = null;

    public void PlayClip()
    {
        if (clip != null)
            SFXManager.Instance.PlayOneShot(clip, SFXManager.DEFAULT_SFX_VOLUME);
    }
}
