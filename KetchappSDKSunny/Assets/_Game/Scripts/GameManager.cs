﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{
    #region Class Declarations

    public enum GameStateEnum
    {
        Game,
        GameOver
    }

    [Serializable]
    public class OnGameStateChanged : UnityEvent<OnGameStateChangedArgs> { }

    [Serializable]
    public class OnGameStateChangedArgs
    {
        public GameStateEnum NewState { get; private set; } = GameStateEnum.Game;
        public GameStateEnum OldState { get; private set; } = GameStateEnum.Game;

        public OnGameStateChangedArgs(GameStateEnum oldState, GameStateEnum newState)
        {
            NewState = newState;
            OldState = oldState;
        }
    }

    #endregion

    public static GameManager Instance { get; private set; } = null;

    public GemController.OnGemTaken onGemTaken = new GemController.OnGemTaken();

    public List<GemController> Gems { get; private set; } = new List<GemController>();

    public OnGameStateChanged onGameStateChanged = new OnGameStateChanged();
    public GameStateEnum GameState
    {
        get { return gameState; }
        set
        {
            GameStateEnum oldState = gameState;
            gameState = value;

            if (onGameStateChanged != null)
                onGameStateChanged.Invoke(new OnGameStateChangedArgs(oldState, value));
        }
    }
    private GameStateEnum gameState = GameStateEnum.Game;


    private void Awake()
    {
        if (Instance == null)
            Instance = this;

#if !UNITY_EDITOR && UNITY_IOS
        Application.targetFrameRate = 60;
#endif
    }

    public void RegisterGem(GemController gem)
    {
        gem.onGemTaken.AddListener(OnGemTaken);

        Gems.Add(gem);
    }

    private void OnGemTaken(GemController.OnGemTakenArgs args)
    {
        Gems.Remove(args.Gem);

        if (onGemTaken != null)
            onGemTaken.Invoke(args);

        CheckVictoryCondition();
    }

    private void CheckVictoryCondition()
    {
        if (Gems.Count == 0)
            GameState = GameStateEnum.GameOver;
    }
}
