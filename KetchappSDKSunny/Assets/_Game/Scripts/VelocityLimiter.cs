﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class VelocityLimiter : MonoBehaviour
{
    public float velocityLimit = 100.0f;

    private new Rigidbody2D rigidbody = null;

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        if (rigidbody.velocity.magnitude > velocityLimit)
            rigidbody.velocity = rigidbody.velocity.normalized * velocityLimit;
    }
}
